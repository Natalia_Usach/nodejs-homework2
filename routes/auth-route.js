/* eslint-disable new-cap */
const express = require('express');
const {registerUser, loginUser} = require('../controllers/auth-controller');
const router = express.Router();

router.post('/api/auth/register', registerUser);
router.post('/api/auth/login', loginUser);

module.exports = router;
